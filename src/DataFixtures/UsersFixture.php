<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class UsersFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');
        $count = 0;
        while ($count < 20) {
            $user = new Users;
            $user->setName($faker->name);
            $manager->persist($user);
            $count++;
        }
        $manager->flush();
    }
    function getOrder()
    {
        return 1;
    }
}
