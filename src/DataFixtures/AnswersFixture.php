<?php

namespace App\DataFixtures;

use Faker;

use App\Entity\Questions;
use App\Entity\Answers;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class AnswersFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');
        $questions = $manager->getRepository(Questions::class)->findAll();

        $count = 0;
        while ($count < 10) {
            $reponse = new Answers;
            $reponse->setStatus($faker->boolean(50));
            $reponse->setContent($faker->text(200));
            
            $reponse->setQuestionId($questions[rand(0, count($questions))]);

            $manager->persist($reponse);
            $count++;
        }
        $manager->flush();
    }
    function getOrder()
    {
        return 3;
    }
}
