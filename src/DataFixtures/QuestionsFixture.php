<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Users;
use App\Entity\Questions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class QuestionsFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');
        $users = $manager->getRepository(Users::class)->findAll();

        $count = 0;
        while ($count < 10) {
            $question = new Questions;
            $question->setTitle($faker->title);
            $question->setContent($faker->text(200));

            $question->setUserId($users[rand(0, count($users))]);

            $manager->persist($question);
            $count++;
        }
        $manager->flush();
    }
    function getOrder()
    {
        return 2;
    }
}
